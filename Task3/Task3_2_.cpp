#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int binaryToDecimal(string);
int calculateSum(string);
int calculateSumFromFile(string);
void calculateSumTest(string);

int main()
{
    calculateSumTest("test.txt");
    return 0;
}

int calculateSum(string source)
{
    int sum = 0;
    string currentNumber = "";

    for (char c : source) {
        if (isdigit(c)) {
            currentNumber += c;
        }
        else {
            if (!currentNumber.empty()) {
                sum += binaryToDecimal(currentNumber);
                currentNumber = "";
            }
        }
    }

    if (!currentNumber.empty()) {
        sum += binaryToDecimal(currentNumber);
    }

    return sum;
}

int calculateSumFromFile(string fileName)
{
    string filePath = "D:\\C++_2сем\\" + fileName;

    ifstream file(filePath);
    if (!file.is_open()) {
        cout << "Unable to open file: " << filePath << endl;
        return 0;
    }

    int sum = 0;
    string line;
    while (getline(file, line)) {
        sum += calculateSum(line);
    }

    file.close();

    return sum;
}

int binaryToDecimal(string binary)
{
    int decimal = 0;
    int power = 1;

    for (int i = binary.length() - 1; i >= 0; i--) {
        if (binary[i] == '1') {
            decimal += power;
        }
        power *= 2;
    }

    return decimal;
}

void calculateSumTest(string fileName)
{
    bool result = calculateSum("1+-0100+*** 1000") == 13;
    cout << "Test for parsing " << (result ? "Passed." : "Failed.") << endl;
    result = calculateSum("1000001+-1000+* 100** 1--- 0000001") == 79;
    cout << "Test for parsing " << (result ? "Passed." : "Failed.") << endl;
    result = calculateSum("10000000000011111+11+*** 1111000111") == 66537;
    cout << "Test for parsing " << (result ? "Passed." : "Failed.") << endl;
    result = calculateSum("1111111111111000011111+-11111111111+*   1111** -22--- ") == 4195885;
    cout << "Test for parsing " << (result ? "Passed." : "Failed.") << endl;
    result = calculateSumFromFile(fileName) == 4262515;
    cout << "Test for parsing " << (result ? "Passed." : "Failed.") << endl;
}